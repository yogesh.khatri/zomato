import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Restaurant from './Restaurant';
import Reviews from './components/Reviews'

const Stack = createStackNavigator();

class HomeScreen extends React.Component {

  // fetching reviews from zomato
  componentDidMount() {
    let id = 100081;
    let url = `https://developers.zomato.com/api/v2.1/restaurant?res_id=${id}`;
    fetch(url, {
      method: 'GET',
      headers: {
        'Content-type': 'application/json',
        'user-key': '5df7f060a68763c55a3e6d9ed76e2df4'
      }
    })
      .then((res) => res.json())
      .then((data) => {
        this.setState({
          restaurant: data
        })
      }).then(() => {
        this.setState({
          isReady: true
        })
      })
  }
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Button
          title="Go to Restaurant"
          onPress={() => this.props.navigation.navigate('Restaurant')}
        />
      </View>
    );

  }
}

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen} options={{ title: 'Overview' }} />
        <Stack.Screen name="Restaurant" component={Restaurant} options={{ title: '' }} />
        <Stack.Screen name="Reviews" component={Reviews} options={{ title: 'Reviews' }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
export default App
