import React, { Component } from 'react'
import { AppLoading } from 'expo';
import { View, ActivityIndicator, Text } from 'react-native'

import RestaurantDetails from './components/RestaurantDetails'

class Restaurant extends Component {
    state = {
        restaurant: {},
        isReady: false
    }

    // fetching data from restaurant on mount component
    componentDidMount() {
        let id = 100081;
        let url = `https://developers.zomato.com/api/v2.1/restaurant?res_id=${id}`;
        fetch(url, {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'user-key': '5df7f060a68763c55a3e6d9ed76e2df4'
            }
        })
            .then((res) => res.json())
            .then((data) => {
                this.setState({
                    restaurant: data
                })
            }).then(() => {
                this.setState({
                    isReady: true
                })
            })
    }
    render() {
        if (!this.state.isReady) {
            return (
                <View>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            )
        }
        return (
            <View>
                {/* {console.log(this.props)} */}
                <RestaurantDetails restaurant={this.state.restaurant} navigation={this.props.navigation.navigate}/>
                <View style={{
                    width: '92%',
                    height: 80,
                    backgroundColor: 'rgba(250,250,250,.95)',
                    justifyContent: 'flex-end',
                    alignItems: 'center',
                    position: 'absolute',
                    bottom: 15,
                    marginHorizontal: '4%',
                    borderRadius: 20,

                }} >
                    <Text style={{ padding: 5, color: 'grey' }}>Add review         Add Photo       Directions       Call</Text>
                </View>
            </View >

        )
    }
}


export default Restaurant
