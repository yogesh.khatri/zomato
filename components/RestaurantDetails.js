import React, { Component } from 'react'
import { View, Text, Image, StyleSheet, ScrollView, Tou, TouchableOpacity, Button } from 'react-native'
import { Dimensions } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

class RestaurantDetails extends Component {
    render() {
        return (
            <ScrollView>
                <Image
                    style={styles.featuredImage}
                    source={{
                        uri: this.props.restaurant['featured_image']
                    }}>
                </Image>

                <View style={styles.ratingAndDetailsView}>
                    <View style={styles.detailsView}>
                        <View>
                            <Text style={styles.name}>{this.props.restaurant['name']}</Text>
                        </View>
                        <View>
                            <Text style={styles.cuisines}>Casual Dining -  {this.props.restaurant['cuisines']}</Text>
                        </View>
                        <View>
                            <Text style={styles.localityVerbose}>{this.props.restaurant['location']['locality_verbose']}</Text>
                        </View>
                        <View>
                            <Text style={styles.timings}>Open at - {this.props.restaurant['timings']}</Text>
                        </View>
                        <View>
                            <Text style={styles.localityVerbose}>Cost for two - &#x20B9;{this.props.restaurant['average_cost_for_two']} (approx.)</Text>
                        </View>
                    </View>
                    {/* ----------------------------------------------------------- */}
                    <View style={styles.ratingBox}>
                        <View style={[styles.rating, { backgroundColor: `#${this.props.restaurant["user_rating"]["rating_color"]}` }]}>
                            <Text style={styles.ratingTextStyle}>{this.props.restaurant["user_rating"]["aggregate_rating"]}</Text>
                        </View>
                        <View style={[styles.votes]}>
                            <Text style={{ display: 'flex', marginLeft: 'auto', marginRight: 'auto' }}>{this.props.restaurant["user_rating"]["votes"]}</Text>
                            <Text style={{ fontSize: 8, marginLeft: 'auto', marginRight: 'auto' }}>REVIEWS</Text>
                        </View>
                    </View>
                </View>
                {/* -------------------------------------------------------------- */}
                <View style={styles.moreInfo}>
                    <Text>More Info</Text>
                    <Text style={{ color: 'rgb(100,100,100)' }}>View additional restaurant details</Text>
                </View>
                {/* --------------------------------------------------------------- */}
                <Text style={{ marginTop: 25, marginBottom: 8, marginLeft: "4%", fontSize: 16 }}>ADDRESS</Text>
                <View style={styles.addressBox}>
                    <View >
                        <Text style={{ paddingLeft: 50, marginTop: 15, borderBottomWidth: .2, paddingBottom: 15 }}>{this.props.restaurant["location"]['address']}</Text>
                        <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around', paddingTop: 5 }}>
                            <Text style={{ color: 'red', borderRightWidth: 1, width: '45%' }}> Copy Loaction</Text>
                            <Text style={{ color: 'red' }}>Get Directions</Text>
                        </View>
                    </View>
                </View>
                {/* ---------------------------------------- */}
                <View style={{ height: 200, borderBottomWidth: .2 }}>
                    <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around', paddingVertical: 15 }}>
                        <Text>MENU</Text>
                        <Text style={{ color: 'red' }}>See full menu</Text>
                    </View>
                </View>
                {/* ----------------------------------------- */}
                <View style={styles.reviewBox}>
                    <Text style={{ letterSpacing: 3, fontSize: 18, paddingLeft: '4%' }}>TRUSTWORTHY REVIEWS</Text>
                    <TouchableOpacity>
                        {/* <Text style={{ color: 'red' }}>See all</Text> */}
                        <Button
                            title="see all"
                            onPress={() => this.props.navigation('Reviews')}
                        />
                        {console.log(this.props)}
                    </TouchableOpacity>
                </View>
                <View style={styles.allRatingsBox}>
                    <View style={[styles.ratingBoxInReviews, { backgroundColor: `#${this.props.restaurant["user_rating"]["rating_color"]}` }]}>
                        <Text style={{ fontSize: 40, color: "white", marginRight: 'auto', marginLeft: 'auto' }}>{this.props.restaurant["user_rating"]["aggregate_rating"]}</Text>
                    </View>
                    <View style={styles.ratingLevel}>
                        <Text style={styles.starText}>1 star</Text>
                        <Text style={styles.starText}>2 star</Text>
                        <Text style={styles.starText}>3 star</Text>
                        <Text style={styles.starText}>4 star</Text>
                        <Text style={styles.starText}>5 star</Text>
                    </View>
                </View>
            </ScrollView>

        )
    }
}


const styles = StyleSheet.create({
    featuredImage: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height * .4,
    },
    ratingAndDetailsView: {
        display: 'flex',
        flexDirection: 'row'
    },
    name: {
        fontSize: 20,
        marginLeft: 5,
        marginTop: 10,
    },
    localityVerbose: {
        marginLeft: 10,
        color: 'rgb(130,130,130)',
        marginTop: 2
    },
    cuisines: {
        marginLeft: 10,
        marginTop: 4,
        color: 'rgb(50,50,50)'
    },
    detailsView: {
        display: 'flex',
        width: '75%',
    },
    timings: {
        marginTop: 5,
        marginLeft: 10,
        color: 'rgb(130,130,130)',

    },
    ratingBox: {
        marginTop: 20,
        marginLeft: 10,
        width: 50,
        height: 80,
    },
    rating: {
        width: 50,
        height: '50%',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
    },
    votes: {
        width: 50,
        height: 40,
        borderWidth: .1,
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10
    },
    moreInfo: {
        marginTop: 15,
        backgroundColor: 'rgb(250,250,250)',
        paddingVertical: 8,
        paddingLeft: 50,
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15
    },
    addressBox: {
        borderWidth: .1,
        width: "92%",
        marginHorizontal: '4%',
        borderRadius: 10,
        paddingBottom: 10
    },
    reviewBox: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        paddingTop: 10,
        paddingBottom: 20
    },
    ratingBoxInReviews: {
        height: 50,
        width: 80,
        borderRadius: 5,
        marginTop: 10,
        display: 'flex',
        alignItems: 'center'
    },
    ratingTextStyle: {
        marginLeft: 'auto',
        marginRight: 'auto',
        fontSize: 20,
        color: 'white'
    },
    allRatingsBox: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        paddingBottom: 50
    },
    ratingLevel: {
        width: '70%',
        alignItems: 'center'
    },
    starText: {
        paddingVertical: 5
    }

})

export default RestaurantDetails
