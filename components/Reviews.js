import React, { Component } from 'react'
import { Text, View } from 'react-native'

export class Reviews extends Component {

    state = {
        reviews: {}
    }
    // fetching reviews from zomato
    componentDidMount() {
        let id = 100081;
        let url = `https://developers.zomato.com/api/v2.1/reviews?res_id=${id}`;
        fetch(url, {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'user-key': '5df7f060a68763c55a3e6d9ed76e2df4'
            }
        })
            .then((res) => res.json())
            .then((data) => {
                this.setState({
                    reviews: data
                })
            }).then(() => {
                this.setState({
                    isReady: true
                })
            })
    }
    render() {
        return (
            <View>
                {console.log(this.props)}
                <View>
                </View>
            </View >
        )
    }
}

export default Reviews
